<?php



function jquery_smoothstate_form_settings() {
  $form = array();

  $form['library'] = array(
    '#type' => 'fieldset',
    '#title' => 'Smooth state ID',
  );

  // Debug mode toggle
  $form['library']['id'] = array(
    '#type' => 'textfield',
    '#title' => t('ID for smoothstate'),
    '#description' => t('Enter the ID for smoothtstae, you can entre multiple by seperating with a comma'),
    '#size' => 30,
  );

  return system_settings_form($form);
}

  ?>
